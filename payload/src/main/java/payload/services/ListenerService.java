package payload.services;

import RMQ.Command;
import RMQ.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import payload.General;
import java.util.LinkedList;

@Service
@Log4j2
public class ListenerService {
    @Autowired
    General general;

    @Autowired
    LinkedList<Command> commands;

    @RabbitListener(queues = "${uuid.value}")
    private void processMessage(Message msg) {
        if ("discovery".equals(msg.getType())) {
            if (msg.getData().get("status").equals("OK")) {
                general.setDiscovered(true);
            }
        } else if ("command".equals(msg.getType())) {
            Command cmd = new Command(msg.getData());
            commands.add(cmd);
        }
        log.info(msg);
    }
}
