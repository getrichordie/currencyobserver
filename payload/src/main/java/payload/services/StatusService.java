package payload.services;

import RMQ.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import payload.General;

import java.util.Calendar;
import java.util.HashMap;

@Service
@Log4j2
public class StatusService {
    @Autowired
    General general;

    @Autowired
    AmqpTemplate template;

    @Scheduled(initialDelay = 0, fixedRate = 5000)
    private void sendStatus() {
        if (!general.isDiscovered()) {
            return;
        }
        Message msg = new Message();
        msg.setType("status");
        msg.setTimestamp(Calendar.getInstance().getTimeInMillis());
        msg.setData(new HashMap<String, String>() {{
            put("source", general.getUID());
            put("status", general.getStatus());
        }});
        template.convertAndSend("StatusQueue", msg);
        log.info(msg);
    }
}
