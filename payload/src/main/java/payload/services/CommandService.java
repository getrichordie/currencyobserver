package payload.services;

import RMQ.Command;
import RMQ.CommandStatus;
import RMQ.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import payload.General;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;

@Service
@Log4j2
public class CommandService {
    @Autowired
    AmqpTemplate template;

    @Autowired
    LinkedList<Command> commandsQueue;

    @Autowired
    General general;

    private Thread thread;

    @PostConstruct
    public void start() {
        thread = new Thread(this::processCommands);
        thread.start();
    }

    private void processCommands() {
        while (1 == 1) {
            while (commandsQueue.isEmpty()) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Command cmd = commandsQueue.pop();
            if ("RECONNECT".equals(cmd.getCommand())) {
                log.info("PROCESSING CMD " + cmd.getCommand());
                cmd.setStatus(CommandStatus.DONE);
                Message resp = new Message();
                resp.setType("command_res");
                resp.setTimestamp(Calendar.getInstance().getTimeInMillis());
                resp.setData(cmd.toHashMap());
                template.convertAndSend("CommandQueue", resp);

                general.setStatus("RECONNECTING");
                Message msg = new Message();
                msg.setType("status");
                msg.setTimestamp(Calendar.getInstance().getTimeInMillis());
                msg.setData(new HashMap<String, String>() {{
                    put("source", general.getUID());
                    put("status", general.getStatus());
                }});
                template.convertAndSend("StatusQueue", msg);

                general.setDiscovered(false);
            }
        }
    }
}
