package payload;

public class General {
    private String status;
    private boolean discovered;
    private String UID;

    public General() {
        status = "UNDEFINED";
        discovered = false;
        UID = "undefined";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDiscovered() {
        return discovered;
    }

    public void setDiscovered(boolean discovered) {
        this.discovered = discovered;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }
}
