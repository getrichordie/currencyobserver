package payload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Calendar;
import java.util.Properties;
import java.util.Random;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
public class Payload {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "payload");
        Properties properties = new Properties();
        properties.setProperty("uuid.value", Integer.toHexString(new Random(Calendar.getInstance().getTimeInMillis()).nextInt()).toUpperCase());

        SpringApplication app = new SpringApplicationBuilder()
                .sources(Payload.class)
                .web(false)
                .properties(properties)
                .build();
        app.run(args);
        //SpringApplication.run(RedisDataStoreService.class, args);
    }
}
