package payload;

import RMQ.Command;
import RMQ.Message;
import com.google.gson.Gson;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

@Configuration
public class AppContext {
    @Value("${spring.rabbitmq.host}")
    private String RMQhost;

    @Value("${uuid.value}")
    private String uid;

    @Bean
    public LinkedList<Command> commandQueue() {
        return new LinkedList<Command>();
    }

    @Bean
    public General general() {
        General general = new General();
        general.setUID(uid);
        return general;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(RMQhost);
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public Queue statusRabbitQueue() {
        return new Queue("StatusQueue", false);
    }

    @Bean
    public Queue discoveryRabbitQueue() {
        return new Queue("DiscoveryQueue", false);
    }

    @Bean
    public Queue commandRabbitQueue() {
        return new Queue("CommandQueue", false);
    }

    @Bean
    public Queue selfRabbitQueue() {
        return new Queue(uid, false);
    }

    @Bean(name="rabbitListenerContainerFactory")
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setMessageConverter(jsonMessageConverter());
        factory.setConcurrentConsumers(4);
        factory.setMaxConcurrentConsumers(8);
        return factory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public MessageConverter jsonMessageConverter()
    {
        return new MessageConverter() {
            @Override
            public org.springframework.amqp.core.Message toMessage(Object o, MessageProperties messageProperties) throws MessageConversionException {
                return new org.springframework.amqp.core.Message(new Gson().toJson(o, Message.class).getBytes(), messageProperties);
            }

            @Override
            public Object fromMessage(org.springframework.amqp.core.Message message) throws MessageConversionException {
                final Object content = new SimpleMessageConverter().fromMessage(message);
                if (content instanceof byte[]) {
                    String json = new String((byte[])content, StandardCharsets.UTF_8);
                    Message msg = new Gson().fromJson(json, Message.class);
                    return msg;
                }
                return content;
            }
        };
    }
}
