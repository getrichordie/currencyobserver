package RMQ;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Type;
import java.util.HashMap;

@Log4j2
public class RMQSender {
    private Channel channel = null;
    private String queue_name = "default";
    private String source_name = "xxx";

    public RMQSender(String queue_name, String source_name) {
        try {
            Connection connection = RMQCore.getNewConnection();
            channel = connection.createChannel();
            this.queue_name = queue_name;
            this.source_name = source_name;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void send(String type, HashMap<String, String> data) throws RuntimeException {
        try {
            Gson gson = new Gson();
            String msg = gson.toJson(new Message(type, data), Message.class);
            log.debug(msg);
            channel.queueDeclare(this.queue_name, false, false, false, null);
            channel.basicPublish("", this.queue_name, null, (msg).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Sending failed.");
        }
    }
}
