package RMQ;

public enum CommandStatus {
    DONE, FAIL, CREATED, PROCESSING, UNDEFINED
}
