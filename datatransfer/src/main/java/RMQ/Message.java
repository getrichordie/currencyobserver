package RMQ;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

public class Message {
    private long timestamp;
    private String type;
    private HashMap<String, String> data;

    public Message() {
        this.timestamp = Calendar.getInstance().getTimeInMillis();
    }

    public Message(String type, HashMap<String, String> data) {
        this.type = type;
        this.data = data;
        this.timestamp = Calendar.getInstance().getTimeInMillis();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return timestamp == message.timestamp &&
                Objects.equals(type, message.type) &&
                Objects.equals(data, message.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, type, data);
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Message{" +
                "timestamp=" + timestamp +
                ", type='" + type + '\'' +
                ", data=" + data +
                '}';
    }
}
