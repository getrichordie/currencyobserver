package RMQ;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public final class RMQCore {
    private static ConnectionFactory factory;

    public RMQCore() {
    }

    public static void init(String host) {
        factory = new ConnectionFactory();
        factory.setHost(host);
    }

    static Connection getNewConnection() throws Exception{
        return factory.newConnection();
    }

}
