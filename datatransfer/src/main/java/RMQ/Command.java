package RMQ;

import java.util.HashMap;

public class Command {
    private String command;
    private int commandId;
    private CommandStatus status;

    public Command() {}

    public Command(HashMap<String, String> map) {
        this.setStatus(CommandStatus.valueOf(map.get("status")));
        this.setCommand(map.get("command"));
        this.setCommandId(Integer.parseInt(map.get("commandId")));
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getCommandId() {
        return commandId;
    }

    public void setCommandId(int commandId) {
        this.commandId = commandId;
    }

    public CommandStatus getStatus() {
        return status;
    }

    public void setStatus(CommandStatus status) {
        this.status = status;
    }

    public HashMap<String, String> toHashMap() {
        var map = new HashMap<String, String>();
        map.put("command", this.command);
        map.put("commandId", Integer.toString(this.commandId));
        map.put("status", this.status.toString());
        return map;
    }
}
