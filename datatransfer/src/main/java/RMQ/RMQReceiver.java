package RMQ;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.*;
import lombok.extern.log4j.Log4j2;

import java.nio.charset.StandardCharsets;

@Log4j2
public class RMQReceiver {
    private Channel channel = null;
    private String queue_name = "default";
    private ReceiveStrategy strategy;
    private int lastHash = -1;

    public RMQReceiver(String queue_name, ReceiveStrategy strategy) {
        try {
            Connection connection = RMQCore.getNewConnection();
            channel = connection.createChannel();
            this.queue_name = queue_name;
            this.strategy = strategy;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() throws Exception {
        channel.queueDeclare(this.queue_name, false, false, false, null);
        log.debug(queue_name + " Waiting for messages.");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                String data = new String(body, StandardCharsets.UTF_8);
                Gson gson = new GsonBuilder().create();
                Message msg = gson.fromJson(data, Message.class);
                if (msg.hashCode() == lastHash) {
                    log.debug(queue_name + " DUPLICATE " + msg.toString());
                } else {
                    log.debug(queue_name + " " + msg.toString());
                    lastHash = msg.hashCode();
                    strategy.handle(msg);
                }
            }
        };
        channel.basicConsume(this.queue_name, true, consumer);
    }

    public void stop() throws Exception {
        channel.basicCancel(this.queue_name);
    }

    public String getQueue_name() {
        return queue_name;
    }
}
