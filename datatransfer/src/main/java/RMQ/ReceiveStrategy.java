package RMQ;

public interface ReceiveStrategy {
    void handle(Message data);
}
