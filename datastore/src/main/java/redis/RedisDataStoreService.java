package redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class RedisDataStoreService {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "redis-data-store");
        SpringApplication app = new SpringApplicationBuilder()
                .sources(RedisDataStoreService.class)
                .web(false)
                .build();
        app.run(args);
        //SpringApplication.run(RedisDataStoreService.class, args);
    }
}
