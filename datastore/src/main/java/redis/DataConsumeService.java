package redis;

import RMQ.Message;
import com.google.gson.GsonBuilder;
import entity.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Log4j2
public class DataConsumeService {
    private long id = 0;

    @Autowired
    AmqpTemplate template;

    @Autowired
    DataRepository dataRepository;

    @RabbitListener(queues = "DataQueue")
    public void consumeData(Message msg) {
        if (!"data".equals(msg.getType())) {
            return;
        }
        log.info(msg);
        Data data = new Data();
        data.setTimestamp(new Date(msg.getTimestamp()));
        data.setData(new GsonBuilder().setPrettyPrinting().create().toJson(msg.getData()));
        data.setId(id++);
        dataRepository.save(data);
    }
}
