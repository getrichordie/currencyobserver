package services.web;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.DetailsGenerator;
import com.vaadin.ui.components.grid.SingleSelectionModel;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import services.background.worker.WorkerDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

@Service
@Theme("valo")
@SpringUI(path = "/")
@Push(PushMode.MANUAL)
public class MainUI extends UI {

    @Value("${back-server.port}")
    private String backServerPort;

    @Autowired
    AmqpTemplate template;

    private HashMap<String, WorkerDTO> workers = new HashMap<>();

    private Grid<WorkerDTO> grid = new Grid<>();
    private TextArea info = new TextArea();
    private ComboBox<String> commands = new ComboBox<>();
    private Button sendBtn = new Button("Send");
    private TextArea commandsHistory = new TextArea();

    private Thread activeWorkersUpdateThread;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        grid.addColumn(WorkerDTO::getUID).setCaption("UID");
        grid.addColumn(WorkerDTO::getStatus).setCaption("STATUS");
        grid.addColumn((w) -> sdf.format(w.getLastUpdate())).setCaption("LASTUPDATE");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        grid.addSelectionListener((SelectionListener<WorkerDTO>) e -> {
            if (!e.getFirstSelectedItem().isPresent()) {
                //info.clear();
                return;
            }
            String UID = e.getFirstSelectedItem().get().getUID();
            StringBuilder builder = new StringBuilder();
            builder.append(UID)
                    .append("\n")
                    .append("Any Extra Info here");
            //DO REST REQUEST OF CMDS HERE
            info.setValue(builder.toString());
            push();
        });

        commands.setEmptySelectionAllowed(false);

        sendBtn.addClickListener((Button.ClickListener) e -> {
            //send data to back
            if (!commands.getSelectedItem().isPresent()) {
                return;
            }
            if (!grid.getSelectionModel().getFirstSelectedItem().isPresent()) {
                return;
            }

            String id = grid.getSelectionModel().getFirstSelectedItem().get().getUID();
            String cmd = commands.getSelectedItem().get();

            RestTemplate restTemplate = new RestTemplate();
            String req = "http://localhost:" + backServerPort + "/workers/" + id + "/sendCommand/" + cmd;
            ResponseEntity<String> responseEntity = restTemplate.exchange(req, HttpMethod.GET, null,
                    new ParameterizedTypeReference<String>() {
                    });
            String res = responseEntity.getBody();

            int maxLen = 5;
            String[] lines = commandsHistory.getValue().split(Pattern.quote("\n"));
            String newLine = commands.getSelectedItem().get() + res;
            StringBuilder builder = new StringBuilder();
            builder.append(newLine).append("\n");
            for(int i = 0; i < lines.length - (lines.length == maxLen ? 1 : 0); i++) {
                builder.append(lines[i]).append("\n");
            }
            commandsHistory.setValue(builder.toString());
        });

        commandsHistory.setEnabled(false);
        commandsHistory.setReadOnly(true);
        commandsHistory.setWidth("400px");
        commandsHistory.setHeight("140px");

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(commands);
        verticalLayout.addComponent(sendBtn);
        verticalLayout.addComponent(commandsHistory);

        HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.addComponent(grid);
        mainLayout.addComponent(info);
        mainLayout.addComponent(verticalLayout);

        setContent(mainLayout);

        activeWorkersUpdateThread = new Thread(this::updateWorkers);
        activeWorkersUpdateThread.start();
        fillCommands();
    }

    public void fillCommands() {
        RestTemplate restTemplate = new RestTemplate();
        String req = "http://localhost:" + backServerPort + "/workers/getAvailableCommands";
        ResponseEntity<ArrayList<String>> responseEntity = restTemplate.exchange(req, HttpMethod.GET, null,
                new ParameterizedTypeReference<ArrayList<String>>() {
                });
        commands.setItems(responseEntity.getBody());
        //commandsList.setData(commands);
    }

    private void updateWorkers() {
        RestTemplate restTemplate = new RestTemplate();
        String req = "http://localhost:" + backServerPort + "/workers/getActiveWorkers";
        while (true) {
            ResponseEntity<ArrayList<WorkerDTO>> responseEntity = restTemplate.exchange(req, HttpMethod.GET, null,
                    new ParameterizedTypeReference<ArrayList<WorkerDTO>>() {
                    });

            ArrayList<WorkerDTO> workersResponse = responseEntity.getBody();
            workers.clear();
            for (WorkerDTO worker : workersResponse) {
                workers.put(worker.getUID(), worker);
            }

            access(() -> {
                //not to lose selection after dataUpdate
                WorkerDTO selected = null;
                if (grid.getSelectionModel().getFirstSelectedItem().isPresent()) {
                    selected = grid.getSelectionModel().getFirstSelectedItem().get();
                }
                grid.setItems(workersResponse);
                if (selected != null && workersResponse.contains(selected)) {
                    grid.select(selected);
                }
                push();
            });

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*if (!workersList.getSelectedItems().isEmpty()) {
            updateStatusArea(workers.get(workersList.getSelectedItems().toArray()[0]));
        }*/
    }
}
