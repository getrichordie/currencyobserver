package services.web;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppContext {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ServletRegistrationBean<SpringVaadinServlet> springVaadinServlet() {
        SpringVaadinServlet servlet = new SpringVaadinServlet();
        ServletRegistrationBean<SpringVaadinServlet> registrationBean = new ServletRegistrationBean<>(servlet, "/*");
        registrationBean.setLoadOnStartup(1);
        registrationBean.setName("VaadinServlet");
        return registrationBean;
    }
}
