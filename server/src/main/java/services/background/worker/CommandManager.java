package services.background.worker;

import RMQ.Command;
import RMQ.CommandStatus;

import java.util.HashMap;
import java.util.Stack;

public class CommandManager {
    private Stack<Command> history = new Stack<>();
    private HashMap<Integer, Command> actual = new HashMap<>();
    private int lastCommandId = 0;

    public Command createCommand(String cmd) {
        lastCommandId++;
        Command command = new Command();
        command.setCommand(cmd);
        command.setCommandId(lastCommandId);
        command.setStatus(CommandStatus.CREATED);
        actual.put(lastCommandId, command);
        return command;
    }

    public void setStatus(int commandId, CommandStatus status) {
        if (actual.containsKey(commandId)) {
            Command cmd = actual.get(commandId);
            cmd.setStatus(status);
            if (status.equals(CommandStatus.DONE) || status.equals(CommandStatus.FAIL)) {
                history.push(cmd);
                actual.remove(commandId);
            }
        }
    }

    public Stack<Command> getHistory() {
        return this.history;
    }

    public HashMap<Integer, Command> getActual() {
        return this.actual;
    }

}