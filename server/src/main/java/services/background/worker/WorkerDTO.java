package services.background.worker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkerDTO {
    private String UID;
    private String status;
    private Date lastUpdate;

    public WorkerDTO() {
    }

    public WorkerDTO(String UID, String status, Date lastUpdate) {
        this.UID = UID;
        this.status = status;
        this.lastUpdate = lastUpdate;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return  "UID='" + UID + '\'' +
                ", status='" + status + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkerDTO workerDTO = (WorkerDTO) o;
        return Objects.equals(UID, workerDTO.UID) &&
                Objects.equals(status, workerDTO.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(UID, status);
    }
}
