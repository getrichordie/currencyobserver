package services.background.services;

import RMQ.Command;
import RMQ.CommandStatus;
import RMQ.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.background.worker.Worker;

import java.util.HashMap;
import java.util.Map;

@Service
@Log4j2
public class CommandService {
    @Autowired
    Map<String, Worker> workers;

    @Autowired
    AmqpTemplate template;

    public Command sendCommand(String UID, String cmd) {
        Worker worker;
        if (!workers.containsKey(UID)) {
            worker = new Worker();
        } else {
            worker = workers.get(UID);
        }
        Command command = worker.getCommandManager().createCommand(cmd);
        command.setStatus(CommandStatus.PROCESSING);
        Message msg = new Message("command", command.toHashMap());
        template.convertAndSend(UID, msg);
        log.info(msg);
        return command;
    }

    @RabbitListener(queues = "CommandQueue")
    public void processCommand(Message msg) {
        //INCLUDE SECURITY HERE!
        if ("command_new".equals(msg.getType())) {
            String dest = msg.getData().get("destination");
            if (!workers.containsKey(dest)) {
                log.error("WORKER " + dest + " NOT FOUND.");
                return;
            }
            String cmd = msg.getData().get("command");
            sendCommand(dest, cmd);
            /*
            Worker worker = workers.get(dest);
            Command command = worker.getCommandManager().createCommand(cmd);
            command.setStatus(CommandStatus.PROCESSING);
            template.convertAndSend(dest, new Message("command", command.toHashMap()));
            */
        }

        if ("command_res".equals(msg.getType())) {
            String src = msg.getData().get("source");
            if (!workers.containsKey(src)) {
                log.error("WORKER " + src + " NOT FOUND.");
                return;
            }
            int commandId = Integer.parseInt(msg.getData().get("commandId"));
            String status = msg.getData().get("status");
            Worker worker = workers.get(src);
            CommandStatus cStatus;
            try {
                cStatus = CommandStatus.valueOf(status);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Unknown STATUS for " + worker.getUID() + " CMDID " + commandId + ": " + status);
                cStatus = CommandStatus.UNDEFINED;
            }
            worker.getCommandManager().setStatus(commandId, cStatus);
        }
        log.info(msg.toString());
    }
}
