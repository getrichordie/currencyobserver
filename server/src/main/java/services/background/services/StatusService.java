package services.background.services;

import RMQ.Command;
import RMQ.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import services.background.worker.Worker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
@Log4j2
public class StatusService {

    private final static int TIMEOUT = 15 * 1000;

    @Autowired
    CommandService commandService;

    @Autowired
    Map<String, Worker> workers;

    @RabbitListener(queues = "StatusQueue")
    public void processStatus(Message msg) {
        if ("status".equals(msg.getType())) {
            String src = msg.getData().get("source");
            String status = msg.getData().get("status");
            if (!workers.containsKey(src)) {
                log.error("UNKNOWN WORKER " + src);
                commandService.sendCommand(src, "RECONNECT");
            } else {
                log.info("WORKER " + src + " STATUS " + status);
                Worker worker = workers.get(src);
                worker.setStatus(status);
                worker.setLastUpdate(Calendar.getInstance().getTimeInMillis());
            }
        }
        //log.info(msg.toString());
    }

    @Scheduled(initialDelay = 0, fixedRate = 5000)
    public void removeNoPingWorkers() {
        var toRemove = new ArrayList<String>();
        for(var entry : workers.entrySet()) {
            if (Calendar.getInstance().getTimeInMillis() - entry.getValue().getLastUpdate() > TIMEOUT) {
                toRemove.add(entry.getKey());
            }
        }
        for(String key : toRemove) {
            workers.remove(key);
            log.info("WORKER " + key + " REMOVED COZ NOPING");
        }
    }
}
