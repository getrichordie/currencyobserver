package services.background.services;

import RMQ.Message;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.background.worker.Worker;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
@Log4j2
public class DiscoveryService {
    @Autowired
    AmqpTemplate template;

    @Autowired
    Map<String, Worker> workers;

    @RabbitListener(queues = "DiscoveryQueue")
    public void processDiscovery(Message msg) {
        if ("discovery".equals(msg.getType())) {
            String src = msg.getData().get("source");
            template.convertAndSend(src, new Message("discovery", new HashMap<>(){{put("status", "OK");}}));
            Worker worker = new Worker();
            worker.setUID(src);
            worker.setStatus("DISCOVERED");
            worker.setLastUpdate(Calendar.getInstance().getTimeInMillis());
            workers.put(src, worker);
        }
        log.info(msg.toString());
    }
}
