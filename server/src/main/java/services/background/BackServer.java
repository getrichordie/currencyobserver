package services.background;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableEurekaClient
@EnableRabbit
@EnableScheduling
public class BackServer {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "back-server");
        SpringApplication.run(BackServer.class, args);
    }
}

