package services.background.controllers;

import RMQ.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import services.background.services.CommandService;
import services.background.worker.Worker;
import services.background.worker.WorkerDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(method = RequestMethod.GET,
        value = "/workers")
public class WorkersController {
    @Autowired
    Map<String, Worker> workers;

    @Autowired
    ArrayList<String> availableCommands;

    @Autowired
    CommandService commandService;

    @RequestMapping(method = RequestMethod.GET,
            value = "/{id}/sendCommand/{command}")
    public String sendCommand(@PathVariable String id, @PathVariable String command) {
        if (!availableCommands.contains(command)) {
            return "INVALID COMMAND.";
        }
        if (!workers.containsKey(id)) {
            return "INVALID WORKER.";
        }
        Command cmd = commandService.sendCommand(id, command);
        return "SENT SUCCESSFUL WITH COMMANDID = " + cmd.getCommandId();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/getActiveWorkers")
    public List<WorkerDTO> getActiveWorkers() {
        var workersArr = new ArrayList<WorkerDTO>();
        for(Worker w : workers.values()) {
            workersArr.add(new WorkerDTO(w.getUID(), w.getStatus(), new Date(w.getLastUpdate())));
        }
        return workersArr;
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/getAvailableCommands")
    public List<String> getAvailableCommands() {
        return availableCommands;
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/getStatus/{id}")
    public String getStatus(@PathVariable String id) {
        if (!workers.containsKey(id)) {
            return null;
        }
        return workers.get(id).getStatus();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/getCommandsHistory/{id}")
    public Stack<Command> getCommandsHistory(@PathVariable String id) {
        if (!workers.containsKey(id)) {
            return null;
        }
        return workers.get(id).getCommandManager().getHistory();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/getProcessingCommands/{id}")
    public Collection<Command> getProcessingCommands(@PathVariable String id) {
        if (!workers.containsKey(id)) {
            return null;
        }
        return workers.get(id).getCommandManager().getActual().values();
    }

    @ExceptionHandler
    public void handleIllegalArgumentException(
            IllegalArgumentException e,
            HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.BAD_REQUEST.value());

    }

}
