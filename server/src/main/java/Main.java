import services.registration.RegistrationServer;
import services.background.BackServer;
import services.web.WebServer;

public class Main {
    public static void main(String[] argv) {
        RegistrationServer.main(argv);
        BackServer.main(argv);
        WebServer.main(argv);
    }
}
